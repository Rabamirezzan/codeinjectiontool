package com.codeinjection.tools.test;

import com.sun.codemodel.JClass;
import com.sun.codemodel.JCodeModel;
import org.junit.Assert;
import org.junit.Test;import java.lang.Exception;import java.lang.System;

/**
 * Created by rramirezb on 15/01/2015.
 */
public class PrimitiveRef {

    @Test
    public void test(){

        JCodeModel codeModel = new JCodeModel();

        boolean exception= false;
        try {
            JClass ref = codeModel.ref(void.class.getName());
            System.out.println(ref);
        } catch (Exception e) {
            e.printStackTrace();
            exception = true;
        }

        Assert.assertFalse("no exception ",exception);

    }
}
