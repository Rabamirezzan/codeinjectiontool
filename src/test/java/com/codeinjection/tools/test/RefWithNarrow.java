package com.codeinjection.tools.test;

import com.codeinjection.tools.CodeModelTool;
import com.sun.codemodel.JClass;
import com.sun.codemodel.JCodeModel;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by rramirezb on 15/01/2015.
 */
public class RefWithNarrow {
    @Test
    public void test(){
        JCodeModel codeModel = new JCodeModel();

        String cls = "java.util.List< a.Raul,    b.Ramirez, c.Bazan>";
        JClass ref;
        ref = CodeModelTool.getTypeElementAsRef(cls, codeModel);



        System.out.println(ref.fullName());

        Assert.assertEquals("Same type", cls.replaceAll(" +", ""), ref.fullName().replaceAll(" +", ""));
    }

    @Test
    public void test01(){
        JCodeModel codeModel = new JCodeModel();

        String cls = "java.util.List< a.Raul,    b.Ramirez, c.Bazan>";
        String[] eTypes = new String[]{"a.Raul","b.Ramirez","c.Bazan"};
        JClass ref;
        ref = CodeModelTool.getTypeElementAsRef(cls, codeModel);


        String[] types = CodeModelTool.getTypeParametersAsString(ref);

        System.out.println(ref.fullName());

        Assert.assertArrayEquals("Same type", eTypes, types);
    }
}
