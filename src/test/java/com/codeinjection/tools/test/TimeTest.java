package com.codeinjection.tools.test;

import org.junit.Assert;
import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by rramirezb on 13/02/2015.
 */
public class TimeTest {

    /**
     * To use TimeZone in Java, its necessary use SimpleDateFormat to display, and GregorianCalendar to make time operations
     */

    @Test
    public void test() throws ParseException {

        final int defaultHourExpected = 8;
        final int africaHourExpected = 17;

        int defaultHour = 8;
         int africaHour = 17;
        //The date instance to configure calendar.
        Date date = new Date();

        //The formatter, server could have another time zone.
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss SSS Z");


        //The calendar, Gregorian Calendar.
        Calendar calendar = new GregorianCalendar();

        //Setting the time;
        calendar.setTime(date);

        //To demostration, setting the calendar to 8 hour of day.
        calendar.set(Calendar.HOUR_OF_DAY, 8);
        calendar.set(Calendar.MINUTE, 0);

        //Getting the default time zone set by operating system
        TimeZone timeZone1 = calendar.getTimeZone();
        //Printing the calendar time.
        System.out.printf("Date %s on timeZone %s\n", sdf.format(calendar.getTime()), timeZone1.getDisplayName());

        TimeZone timeZone2 = TimeZone.getTimeZone("Africa/Asmara");

        defaultHour = calendar.get(Calendar.HOUR_OF_DAY);

        calendar.setTimeZone(timeZone2);
        sdf.setTimeZone(timeZone2);

        africaHour =  calendar.get(Calendar.HOUR_OF_DAY);

        String time = sdf.format(calendar.getTime());
        System.out.printf("Date %s on timeZone %s\n", time, timeZone2.getDisplayName());


        sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss SSS Z");
        Date dateAfterTimeZoneSet = sdf.parse(time);
        System.out.printf("%s\n", dateAfterTimeZoneSet);

        Assert.assertEquals("default hour", defaultHourExpected, defaultHour);
        Assert.assertEquals("africa hour", africaHourExpected, africaHour);

        //We need to have caution with Date class; dateAfterTimeZoneSet
        //was created with a SimpleDateFormat with +3 time zone, but Date still have the operating system time zone.
        int dateHours = dateAfterTimeZoneSet.getHours();
        Assert.assertEquals("default hour", defaultHourExpected, dateHours);

    }
}
