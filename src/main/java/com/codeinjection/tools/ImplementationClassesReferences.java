package com.codeinjection.tools;

import java.lang.String; /**
 * Created by rramirezb on 15/01/2015.
 */
public interface ImplementationClassesReferences {
    public String getImplementation(String s);
}
