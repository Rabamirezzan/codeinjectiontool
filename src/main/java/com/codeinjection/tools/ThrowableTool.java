package com.codeinjection.tools;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

/**
 * Created by rramirezb on 13/01/2015.
 */
public class ThrowableTool {

    public static String getStackTrace(Throwable t){
        String result = "";
        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            PrintStream s= new PrintStream(out);
            t.printStackTrace(s);
            result = out.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }
}
