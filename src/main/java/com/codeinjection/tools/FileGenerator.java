package com.codeinjection.tools;


import com.codeinjection.tools.data.FileData;
import com.codeinjection.tools.log.Log;
import com.sun.codemodel.*;
import com.sun.codemodel.writer.SingleStreamCodeWriter;

import javax.annotation.processing.ProcessingEnvironment;
import javax.tools.JavaFileObject;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.Writer;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by rramirezb on 29/12/2014.
 */
public class FileGenerator {
    private FileCache cache;

    {
        cache = new FileCache();
    }

    public JCodeModel getCodeModel(String cls) {
        FileData file;
        file = getFileData(cls);
        return file.getCodeModel();
    }

    public List<JCodeModel> getCodeModelList(){
        Collection<FileData> files = cache.getFiles();
        List<JCodeModel> list=  new ArrayList<JCodeModel>();
        for (Iterator<FileData> iterator = files.iterator(); iterator.hasNext(); ) {
            FileData next = iterator.next();
            JCodeModel codeModel = next.getCodeModel();
            list.add(codeModel);
        }
        return list;
    }

    public FileData getFileData(String cls) {
        FileData file = cache.get(cls);
        if (file == null) {
            file = new FileData(cls, new JCodeModel(), false);

            cache.put(cls, file);
        }
        return file;
    }

    public JDefinedClass getJDefinedClass(int mods, String cls, ClassType classType, boolean createInLastRound) {
        return getJDefinedClass(mods, cls, classType, createInLastRound, null);
    }

    public JDefinedClass getJDefinedClass(int mods, String cls, ClassType classType, boolean createInLastRound, AtomicBoolean exists) {
        FileData file = getFileData(cls);
        file.setCreateInLastRound(createInLastRound);
        JCodeModel codeModel = file.getCodeModel();
        JDefinedClass definedClass = null;
        try {
            if (exists == null) {
                exists = new AtomicBoolean();
            }
            definedClass = codeModel._class(mods, cls, classType);
            exists.set(false);
        } catch (JClassAlreadyExistsException e) {
            exists.set(true);
            definedClass = codeModel._getClass(cls);
        }
        return definedClass;
    }

    public void generateFiles(ProcessingEnvironment processingEnv, List<Class<? extends Annotation>> annotations) {

        Collection<FileData> files = cache.getFiles();


        boolean hasCreateCurrentRound = hasCurrentRoundCreationFile(files);
        Iterator<FileData> it = files.iterator();

        if(annotations==null){
            annotations = new ArrayList<Class<? extends Annotation>>();
        }
        if (hasCreateCurrentRound) {
            while (it.hasNext()) {
                try {
                    FileData file = it.next();
                    if (!file.isCreateInLastRound()) {
                        if(annotations!=null) {
                            for (Class<? extends Annotation> a : annotations) {
                                addAnnotation(file, a);
                            }
                        }


                        Writer source = getSource(processingEnv, file.getCls());
                        Writer writer = source;
                        Log.i("BUILD", "Building %s", file.getCls());
                        build(file.getCodeModel(), writer);
                        it.remove();
                    }
                } catch (Exception e) {
                    e.printStackTrace();

                }
            }
        } else {
            while (it.hasNext()) {
                FileData file = it.next();
                for (Class<? extends Annotation> a : annotations) {
                    addAnnotation(file, a);
                }

                try {

                    Writer source = getSource(processingEnv, file.getCls());
                    Writer writer = source;
                    Log.i("BUILD", "Building %s, last round.", file.getCls());
                    build(file.getCodeModel(), writer);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                it.remove();

            }
        }
    }

    public static Writer getSource(ProcessingEnvironment processingEnv, String qualifiedNameClass) throws IOException {
        //File file = new File(sourceOutput, qualifiedNameClass.replace('.', '/').concat(".java"));
        Writer source = null;


        //Logger.log("el file a crear es %s", file.getAbsolutePath());
        //if (file.exists()) {
        //source = new PrintWriter(new OutputStreamWriter(new FileOutputStream(file)));

        //} else {
        JavaFileObject jfo = processingEnv.getFiler().createSourceFile(qualifiedNameClass);
        source = jfo.openWriter();
        //}


        return source;
    }

//    public static JavaFileObject getSource(ProcessingEnvironment processingEnv, String qualifiedNameClass) {
//        JavaFileObject source = null;
//        try {
//
//            source = processingEnv.getFiler().createSourceFile(qualifiedNameClass);
//        } catch (IOException e) {
////            try {
////                FileObject _source = processingEnv.getFiler().getResource(StandardLocation.SOURCE_OUTPUT, getPackageName(qualifiedNameClass), getClassName(qualifiedNameClass) + "");
////                boolean deleted = false; //new File(source.toUri()).delete();
////
////                source = processingEnv.getFiler().createSourceFile(qualifiedNameClass);
////
////            } catch (IOException e2) {
////
////                e.printStackTrace();
////            }
//        }
//        return source;
//    }

    public static String getClassName(String qualifiedName) {
        String result = "";
        if (!isNullOrEmptyString(qualifiedName)) {
            int index = qualifiedName.lastIndexOf('.');
            result = qualifiedName.substring(index + 1, qualifiedName.length());
        }


        return result;
    }

    private static boolean isNullOrEmptyString(String value) {

        boolean result = value == null || value.equals("");
        return result;
    }

    private static CharSequence getPackageName(String qualifiedNameClass) {
        return qualifiedNameClass.substring(0, qualifiedNameClass.lastIndexOf('.'));
    }

    public static String getCode(JCodeModel codeModel) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        String code = "";
        try {
            codeModel.build(new SingleStreamCodeWriter(out));
            code = out.toString().replaceAll("^[^\n]*-+[^\n]*-+[^\n]*\n|^\n$", "");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return code;
    }

    public static void build(JCodeModel codeModel, Writer writer) throws IOException {


        String code = getCode(codeModel);

        writer.append(code);
        writer.flush();
        writer.close();
        System.gc();


    }


    public static boolean hasCurrentRoundCreationFile(Collection<FileData> files) {
        Iterator<FileData> it = files.iterator();
        boolean result = false;
        while (it.hasNext()) {
            result = !it.next().isCreateInLastRound();
            if (result) {
                break;
            }
        }
        return result;
    }

    public static JDefinedClass getJDefinedClass(FileData file) {
        return file.getCodeModel()._getClass(file.getCls());
    }

    public static void addAnnotation(FileData file, Class<? extends Annotation> ann) {

        JDefinedClass cls = getJDefinedClass(file);
        Iterator<JAnnotationUse> it = cls.annotations().iterator();
        boolean add = true;
        while (it.hasNext()) {
            if (it.next().getAnnotationClass().fullName().equals(ann.getName())) {
                add = false;
                break;
            }
        }
        if (add) {
            cls.annotate(ann);
        }
    }
}
