package com.codeinjection.tools;

import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.PrimitiveType;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.ElementFilter;
import java.util.List;

/**
 * Created by rramirezb on 13/01/2015.
 */
public class ElementTool {
    public static boolean isInterface(TypeElement typeElement) {
        return typeElement.getKind().isInterface();
    }



    public static boolean isDeclaredType(VariableElement e) {
        return e.asType() instanceof DeclaredType;
    }
    public static boolean isPrimitiveType(VariableElement e) {
        return e.asType() instanceof PrimitiveType;
    }

//    public static TypeElement getAsTypeElement(VariableElement e) {
//        TypeElement type = null;
//        if (e.asType() instanceof DeclaredType) {
//            DeclaredType _type = (DeclaredType) e.asType();
//            type = (TypeElement) _type.asElement();
//        }else if(e.asType() instanceof PrimitiveType){
//
//        }
//
//        return type;
//    }

    public static String getTypeName(VariableElement e) {

        String name = "";
        if (e.asType() instanceof DeclaredType) {

            name = asTypeElement(e.asType()).toString();
        }else if(e.asType() instanceof PrimitiveType){
            name = asPrimitiveType(e.asType()).toString();
        }

        return name;
    }

//    public static PrimitiveType getAsPrimitiveType(VariableElement e){
//        PrimitiveType type =null;
//        if(e.asType() instanceof PrimitiveType){
//            type = (PrimitiveType) e.asType();
//        }
//        return type;
//    }

    public static String getImplementationPackage(VariableElement e) {
        return ((PackageElement) e.getEnclosingElement().getEnclosingElement()).getQualifiedName().toString();
    }

    public static String getImplementationPackage(TypeElement e) {
        return ((PackageElement) e.getEnclosingElement()).getQualifiedName().toString();
    }

    public static boolean hasInterface(TypeElement type, String simpleNameInterface) {
        List<? extends TypeMirror> interfaces = type.getInterfaces();

        boolean result = hasInterface(interfaces, simpleNameInterface);
        return result;
    }

    public static boolean hasInterface(TypeElement type, Class<?> _interface) {
        boolean result = hasInterface(type, _interface.getName());
        return result;
    }

    private static boolean hasInterface(List<? extends TypeMirror> interfaces, String qualifiedName) {
        boolean result = false;
        for (TypeMirror mirror : interfaces) {

            if (hasInterface(mirror, qualifiedName)) {
                result = true;

            } else {
                result = hasInterface(asTypeElement(mirror), qualifiedName);
            }
            if(result){
                break;
            }

        }
        return result;
    }

    public static TypeElement asTypeElement(TypeMirror mirror) {
        TypeElement type = null;
        if (mirror instanceof DeclaredType) {
            type = (TypeElement) ((DeclaredType) mirror).asElement();
        }

        return type;
    }

    public static PrimitiveType asPrimitiveType(TypeMirror mirror) {
        PrimitiveType type = null;
        if (mirror instanceof PrimitiveType) {
            type = (PrimitiveType) ( mirror);
        }

        return type;
    }

    private static boolean hasInterface(TypeMirror mirror, String qualifiedName) {
        TypeElement _interface = asTypeElement(mirror);

        boolean result = false;
        result = _interface.getQualifiedName().equals(qualifiedName);
        result = true;

        return result;
    }

    public static List<ExecutableElement> getMethods(TypeElement type) {
        return ElementFilter.methodsIn(type.getEnclosedElements());
    }

    public static TypeElement getReturnTypeAsTypeElement(ExecutableElement e) {
        TypeMirror mirror = e.getReturnType();
        return asTypeElement(mirror);
    }

    public static String getReturnTypeAsPrimitiveType(ExecutableElement e) {
        TypeMirror mirror = e.getReturnType();
        return getReturnTypeAsType(mirror);
    }

    public static String getReturnTypeName(TypeMirror mirror) {
        String name="";
        if (mirror instanceof DeclaredType) {
            TypeElement type = asTypeElement(mirror);

            name = type.getQualifiedName().toString();

        }else{
            if(mirror.getKind().isPrimitive()){
                PrimitiveType pt = asPrimitiveType(mirror);
                name = pt.toString();
            }

        }
        return name;
    }

    public static String getReturnTypeAsType(TypeMirror mirror) {
        String name="";
        if (mirror instanceof DeclaredType) {
            TypeElement type = (TypeElement) ((DeclaredType) mirror).asElement();
            name = type.getQualifiedName().toString();


        }else{
            if(mirror.getKind().isPrimitive()){
                PrimitiveType pt = ((PrimitiveType) mirror);
                name = pt.toString();
            }

        }
        return name;
    }

    public static String getReturnTypeName(ExecutableElement e) {
        String name = getReturnTypeAsType(e.getReturnType());
//        String returnTypeName = r != null ? r.getQualifiedName().toString() : "void";
        return name;
    }


    public static String getImplementedName(String implementationPackage, String interfaceName, String prefix, String suffix) {
        return String.format("%s.%s%s%s",implementationPackage, prefix, interfaceName, suffix);
    }

    public static String getQualifiedName(TypeElement e) {
        return e.getQualifiedName().toString();
    }

    public static String getPropertyName(String method) {
        String property = method.replaceFirst("(get)", "");
        if(!isSecondCharDigitOrUpperCase(property)) {
            property = toLowerFirstLetter(property);
        }
        return property;
    }

    public static boolean isSecondCharDigitOrUpperCase(String text){
        return isSecondCharUpperCase(text)||isSecondCharDigit(text);
    }
    public static boolean isSecondCharDigit(String line){
        return line.length()>=2? Character.isDigit(line.charAt(1)):false;
    }
    public static boolean isSecondCharUpperCase(String line){
        return line.length()>=2? Character.isUpperCase(line.charAt(1)):false;
    }

    public static String toUpperFirstLetter(String text){
        return text.substring(0,1).toUpperCase().concat(text.substring(1, text.length()));
    }

    public static String toLowerFirstLetter(String text){
        return text.substring(0,1).toLowerCase().concat(text.substring(1, text.length()));
    }

    public static String getSimpleClassName(String qualifiedName) {
        String qn = qualifiedName;//CodeModelTool.getTypeWithoutTypeParameters(qualifiedName);
        return qn.substring(qn.lastIndexOf('.')+1, qn.length());
    }

}
