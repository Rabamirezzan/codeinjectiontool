package com.codeinjection.tools;

import com.sun.codemodel.JFieldVar;

/**
 * Created by rramirezb on 27/02/2015.
 */
public interface AnnotationParamSetter {
    public void onAnnotate(JFieldVar field);
}
