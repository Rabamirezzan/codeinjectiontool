package com.codeinjection.tools.data;

import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import java.lang.annotation.Annotation;

/**
 * Created by rramirezb on 14/01/2015.
 */
public class DataTypeElement<A extends Annotation> implements DataElement {

    private TypeElement element;
    private Element annotationOwner;
    private Class<A> annotationClass;

    public DataTypeElement(TypeElement element, Element annotationOwner, Class<A> annotationClass) {
        this.element = element;
        this.annotationOwner = annotationOwner;
        this.annotationClass = annotationClass;
    }

    public TypeElement getElement() {
        return element;
    }

    public Element getAnnotationOwner() {
        return annotationOwner;
    }

    public A getAnnotation(){
        return annotationOwner.getAnnotation(annotationClass);
    }
}
