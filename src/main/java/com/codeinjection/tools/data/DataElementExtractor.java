package com.codeinjection.tools.data;

import javax.lang.model.element.Element;

/**
 * Created by rramirezb on 14/01/2015.
 */
public interface DataElementExtractor<E extends Element, D extends DataElement> {
    public D extract(E element);
}
