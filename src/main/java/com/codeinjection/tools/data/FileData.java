package com.codeinjection.tools.data;

import com.sun.codemodel.JCodeModel;import java.lang.String;

/**
 * Created by rramirezb on 29/12/2014.
 */
public class FileData {
    private final String cls;
    private final JCodeModel codeModel;
    private boolean createInLastRound;

    public FileData(String cls, JCodeModel codeModel, boolean createInLastRound ) {
        this.codeModel = codeModel;
        this.cls = cls;
        this.createInLastRound = createInLastRound;
    }

    public String getCls() {
        return cls;
    }



    public JCodeModel getCodeModel() {
        return codeModel;
    }

    public boolean isCreateInLastRound() {
        return createInLastRound;
    }

    public void setCreateInLastRound(boolean createInLastRound) {
        this.createInLastRound = createInLastRound;
    }
}
