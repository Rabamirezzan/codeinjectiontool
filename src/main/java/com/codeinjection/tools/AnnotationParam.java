package com.codeinjection.tools;

/**
 * Created by rramirezb on 27/02/2015.
 */
public class AnnotationParam<T> {
    private final String name;
    private final T value;

    public AnnotationParam(String name, T value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public T getValue() {
        return value;
    }
}
