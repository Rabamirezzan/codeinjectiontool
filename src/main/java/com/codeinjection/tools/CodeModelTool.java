package com.codeinjection.tools;

import com.codeinjection.tools.log.LocalFileStream;
import com.codeinjection.tools.log.Log;
import com.sun.codemodel.JClass;
import com.sun.codemodel.JCodeModel;
import com.sun.codemodel.JMod;

import javax.lang.model.element.*;
import javax.lang.model.type.TypeMirror;
import java.util.*;

/**
 * Created by rramirezb on 13/01/2015.
 */
public class CodeModelTool {
    public static int getMods(Element element, Modifier... ignored) {
        Set<Modifier> modifiers = element.getModifiers();
        Iterator<Modifier> it = modifiers.iterator();
        int mods = 0;
        List<Modifier> modifiersList = Arrays.asList(ignored);
        while (it.hasNext()) {
            Modifier modifier = it.next();
            if(modifiersList.contains(modifier)) continue;
            switch (modifier) {
                case PUBLIC:
                    mods |= JMod.PUBLIC;
                    break;
                case PROTECTED:
                    mods |= JMod.PROTECTED;
                    break;
                case PRIVATE:
                    mods |= JMod.PRIVATE;
                    break;
                case ABSTRACT:
                    mods |= JMod.ABSTRACT;
                    break;
                case DEFAULT:

                    break;
                case STATIC:
                    mods |= JMod.STATIC;
                    break;
                case FINAL:
                    mods |= JMod.FINAL;
                    break;
                case TRANSIENT:
                    mods |= JMod.TRANSIENT;
                    break;
                case VOLATILE:
                    mods |= JMod.VOLATILE;
                    break;
                case SYNCHRONIZED:
                    mods |= JMod.SYNCHRONIZED;
                    break;
                case NATIVE:
                    mods |= JMod.NATIVE;
                    break;
                case STRICTFP:

                    break;
            }
        }

        return mods;

    }

    public static JClass getRef(TypeElement element, JCodeModel codeModel){
        return codeModel.ref(element.getQualifiedName().toString());
    }

    public static String getTypeWithoutTypeParameters(String cls) {
        return cls.replaceAll(" *<.*> *", "");
    }

    public static String[] getTypeParametersAsString(String cls) {
        String parameters = cls.replaceAll(".*< *| *>.*", "");
        String[] result = new String[0];
        if (cls.contains("<")) {
            result = parameters.split(" *, *");
        }
        return result ;
    }

    public static String[] getTypeParametersAsString(JClass cls) {
        List<JClass> tps = cls.getTypeParameters();
        String[] parameters = new String[tps.size()];

        int i = 0;
        for (JClass t :tps){
            parameters[i++] = t.fullName();
        }
        return parameters;
    }


    public static JClass getReturnTypeAsJClass(ExecutableElement e, JCodeModel codeModel) {

        boolean primitive = e.getReturnType().getKind().isPrimitive();
        Log.i("getReturnTypeAsJClass ", "tp of %s, primitive %s", e.getReturnType(), primitive);
        JClass jclass;
        if(primitive){
            jclass = codeModel.ref(e.getReturnType().toString());
        }else{
            jclass =getTypeElementAsRef(e.getReturnType().toString(), codeModel);
        }
        return jclass ;
    }

    public static JClass getVariableTypeAsJClass(VariableElement e, JCodeModel codeModel) {

        boolean primitive = ElementTool.isPrimitiveType(e);

        JClass jclass;
        if(primitive){
            jclass = codeModel.ref(ElementTool.asPrimitiveType(e.asType()).toString());
        }else{
            jclass =getTypeElementAsRef(ElementTool.getTypeName(e).toString(), codeModel);
        }
        return jclass ;
    }

    public static JClass[] getParameterTypeWithTypeParameters(VariableElement element, JCodeModel codeModel) {
        TypeMirror type = element.asType();
        JClass[] ref = new JClass[]{};

        ref = getTypeElementAsRefWithParameterTypes(type.toString(), codeModel);

        return ref;
    }

    public static JClass[] getReturnTypeAsJClassWithTypeParameters(ExecutableElement e, JCodeModel codeModel) {

        boolean primitive = e.getReturnType().getKind().isPrimitive();
        Log.i("getReturnTypeAsJClass ", "tp of %s, primitive %s", e.getReturnType(), primitive);
        JClass[] jclass = new JClass[]{};
        if(primitive){
            JClass jc = codeModel.ref(e.getReturnType().toString());
            jclass = new JClass[]{jc};
        }else{
            jclass =getTypeElementAsRefWithParameterTypes(e.getReturnType().toString(), codeModel);
        }
        return jclass ;
    }
    static{
        Log.getInstance().setMessager(new LocalFileStream());
        Log.i("INIT", "iniciando log");
    }
    public static JClass getTypeElementAsRef(String type, JCodeModel codeModel){
        String cls = getTypeWithoutTypeParameters(type);
        String[] tps =  getTypeParametersAsString(type);
        JClass ref = codeModel.ref(cls);


        Log.i("getTypeElementAsRef ", "tp of %s: %s", cls, Arrays.asList(tps));
        for (int i = 0 ; i<tps.length; i++) {
            String tp = tps[i];
            Log.i("getTypeElementAsRef ", "narrowing %s with %s", cls, tp);
            ref = ref.narrow(getTypeElementAsRef(tp, codeModel));

        }

        return ref;
    }

    public static JClass getTypeElementAsRef(JCodeModel codeModel, JClass cls, JClass... typeParameters){
        JClass ref = cls;
        for (int i = 0; i < typeParameters.length; i++) {
            JClass typeParameter = typeParameters[i];
            ref =ref.narrow(typeParameter);
        }
        return ref;
    }
    public static JClass[] getTypeElementAsRefWithParameterTypes(String type, JCodeModel codeModel){
        String cls = getTypeWithoutTypeParameters(type);
        String[] tps =  getTypeParametersAsString(type);
        JClass ref = codeModel.ref(cls);

        List<JClass> refs = new ArrayList<JClass>();

        refs.add(ref);
        Log.i("getTypeElementAsRef ", "tp of %s: %s", cls, Arrays.asList(tps));
        for (int i = 0 ; i<tps.length; i++) {
            String tp = tps[i];
            Log.i("getTypeElementAsRef ", "narrowing %s with %s", cls, tp);
            JClass w = getTypeElementAsRef(tp, codeModel);
            refs.add(w);
            ref = ref.narrow(w);

        }

        return refs.toArray(new JClass[]{});
    }

    public static boolean implementsInterface(JClass type, Class<?> _interface) {
        Iterator<JClass> impls = type._implements();

        boolean result = false;
        while (impls.hasNext()) {
            JClass i = impls.next();
            result = i.fullName().equals(_interface.getName());

            if(!result){
               result = implementsInterface(type._implements(), _interface);
            }
            if (result) {
                break;
            }
        }
        return result;
    }

    public static boolean implementsInterface(Iterator<JClass> jClassIterator,  Class<?> _interface) {

        boolean result = false;
        while (jClassIterator.hasNext()) {
            JClass next =  jClassIterator.next();
            result = implementsInterface(next, _interface);
            if(result) break;
        }
        return result;
    }
}
