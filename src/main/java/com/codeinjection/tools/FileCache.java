package com.codeinjection.tools;


import com.codeinjection.tools.data.FileData;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by rramirezb on 29/12/2014.
 */
public class FileCache {
    private Map<String, FileData> files;

    {
        files = new HashMap<String, FileData>();
    }
    public void put(String cls, FileData file){
        files.put(cls,file);
    }

    public FileData get(String cls){
        return files.get(cls);
    }

    public Collection<FileData> getFiles(){
        return files.values();
    }

    public void remove(String cls) {
        files.remove(cls);
    }
}
