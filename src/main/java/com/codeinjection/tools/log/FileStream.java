package com.codeinjection.tools.log;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Element;
import javax.tools.Diagnostic;
import javax.tools.FileObject;
import javax.tools.StandardLocation;
import java.io.IOException;
import java.io.Writer;

/**
 * Created by rramirezb on 13/01/2015.
 */
public class FileStream implements Stream {
    String filename;
    private Writer resourceWriter;

    public FileStream(String filename) {
        this.filename = filename;
    }

    @Override
    public void setProcessingEnv(ProcessingEnvironment processingEnv) {
        try {
            FileObject resource;
            if (resourceWriter == null) {
                resource = processingEnv.getFiler().createResource(StandardLocation.SOURCE_OUTPUT, "", filename);
                resourceWriter = resource.openWriter();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void printMessage(Diagnostic.Kind kind, Element element, String message) {
        try {
            if(resourceWriter!=null) {
                String msg = "";
                if(element==null){
                    msg = String.format("%s> %s\n", kind, message);
                }else{
                    msg = String.format("%s> element: %s, %s\n", kind, element.getSimpleName(), message);
                }
                resourceWriter.append(msg);
                resourceWriter.flush();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void open() {

    }

    @Override
    public void close() {
        try {
            if(resourceWriter!=null)
            resourceWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
