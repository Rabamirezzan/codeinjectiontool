package com.codeinjection.tools.log;

import javax.lang.model.element.Element;
import javax.tools.Diagnostic;

/**
 * Created by rramirezb on 13/01/2015.
 */
public class Log {
    private Stream messager;
    private static Log instance;

    public static Log getInstance() {
        if (instance == null) {
            instance = new Log();
        }
        return instance;
    }

    private Log() {
    }

    public void setMessager(Stream messager) {
        if (this.messager == null) {
            this.messager = messager;
            this.messager.open();
        }
    }



    public static void print(Diagnostic.Kind kind, Element element, String tag, String format, Object... args) {
        getInstance().messager.printMessage(kind, element, String.format("[%s] %s", tag, String.format(format, args)));
    }

    public static void i(Element element, String tag, String format, Object... args) {
        print(Diagnostic.Kind.NOTE, element, tag, format, args);
    }

    public static void i( String tag, String format, Object... args) {
        i( null, tag, format, args);
    }

    public static void w(Element element, String tag, String format, Object... args) {
        print(Diagnostic.Kind.WARNING, element, tag, format, args);
    }

    public static void w( String tag, String format, Object... args) {
        w( null, tag, format, args);
    }

    public static void e(Element element, String tag, String format, Object... args) {
        print(Diagnostic.Kind.ERROR, element, tag, format, args);
    }

    public static void e(String tag, String format, Object... args) {
        e( null, tag, format, args);
    }

    public static void close() {
        getInstance().messager.close();
    }

}
