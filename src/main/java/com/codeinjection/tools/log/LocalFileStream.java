package com.codeinjection.tools.log;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Element;
import javax.tools.Diagnostic;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by rramirezb on 15/01/2015.
 */
public class LocalFileStream implements  Stream {

    PrintWriter writer;

    {



    }

    @Override
    public void open() {
        try {
            if(writer==null)
            writer = new PrintWriter(new BufferedWriter(new FileWriter("E:/Temp/code-injection-tools.txt", true)));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setProcessingEnv(ProcessingEnvironment processingEnv) {

    }

    @Override
    public void printMessage(Diagnostic.Kind kind, Element element, String message) {
        writer.println(String.format("[%s] > %s", kind, message));
        writer.flush();
    }

    @Override
    public void close() {
        try {
            writer.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
