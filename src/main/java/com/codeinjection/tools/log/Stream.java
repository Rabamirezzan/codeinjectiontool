package com.codeinjection.tools.log;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Element;
import javax.tools.Diagnostic;

/**
 * Created by rramirezb on 13/01/2015.
 */
public interface Stream {
    public void setProcessingEnv(ProcessingEnvironment processingEnv);

    void open();
    void printMessage(Diagnostic.Kind kind, Element element, String message);

    void close();
}
