package com.codeinjection.tools;

import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Created by rramirezb on 28/12/2014.
 */
public class ValidDataTypes {
    {

    }
    public static boolean isValidSerializableType(String qualifiedName){
        boolean result = false;
        result = BigDecimal.class.getName().equals(qualifiedName);
        if(!result){
            result = Integer.class.getName().equals(qualifiedName)|| int.class.getName().equals(qualifiedName);
        }
        if(!result){
            result = Long.class.getName().equals(qualifiedName)|| long.class.getName().equals(qualifiedName);
        }
        if(!result){
            result = Double.class.getName().equals(qualifiedName)|| double.class.getName().equals(qualifiedName);
        }
        if(!result){
            result = Float.class.getName().equals(qualifiedName)|| float.class.getName().equals(qualifiedName);
        }
        if(!result){
            result = Short.class.getName().equals(qualifiedName)|| short.class.getName().equals(qualifiedName);
        }
        if(!result){
            result = Boolean.class.getName().equals(qualifiedName)|| boolean.class.getName().equals(qualifiedName);
        }
        if(!result){
            result = Byte.class.getName().equals(qualifiedName)|| byte.class.getName().equals(qualifiedName);
        }
        if(!result){
            result = Character.class.getName().equals(qualifiedName)|| char.class.getName().equals(qualifiedName);
        }
        if(!result){
            result = Date.class.getName().equals(qualifiedName)|| java.sql.Date.class.getName().equals(qualifiedName);
        }

        if(!result){
            result = Timestamp.class.getName().equals(qualifiedName);
        }
        if(!result){
            result = UUID.class.getName().equals(qualifiedName);
        }
        if(!result){
            try {
                result = Annotation.class.isAssignableFrom(Class.forName(qualifiedName));
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }

        if(!result){
            try {
                result = Serializable.class.isAssignableFrom(Class.forName(qualifiedName));
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        return result;
    }


    public static boolean isCollection( String qualifiedName){
        boolean result = false;
        try {
            Class<?> cls = Class.forName(CodeModelTool.getTypeWithoutTypeParameters(qualifiedName));
            result = Collection.class.isAssignableFrom(cls);
        } catch (ClassNotFoundException e) {
            System.err.println(String.format("Error on isCollection: %s", e));
        }
        return result;
    }

    public static boolean isList( String qualifiedName){
        boolean result = false;
        try {

            Class<?> cls = Class.forName(CodeModelTool.getTypeWithoutTypeParameters(qualifiedName));

            result = List.class.isAssignableFrom(cls);
        } catch (ClassNotFoundException e) {
            System.err.println(String.format("Error on isList: %s", e));
            //e.printStackTrace();
        }
        return result;
    }
}
