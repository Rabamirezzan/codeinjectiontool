package com.codeinjection.tools;

import com.codeinjection.tools.data.DataElement;
import com.codeinjection.tools.data.DataElementExtractor;

import javax.lang.model.element.Element;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * Created by rramirezb on 16/01/2015.
 */
public class GeneratorTool {
    public static <E extends Element, D extends DataElement> List<D> getByElements(Set<E> elements, DataElementExtractor<E, D> extractor) {
        Iterator<E> it = elements.iterator();
        List<D> interfaces = new ArrayList<D>();
        while (it.hasNext()) {
            E element = it.next();
            D data = extractor.extract(element);
            if(data!=null){
                interfaces.add(data);
            }
        }

        return interfaces;
    }
}
